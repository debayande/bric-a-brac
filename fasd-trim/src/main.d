import std.array;
import std.file;
import std.logger;
import std.path;
import std.process;
import std.stdio;

void main() {
    auto fasdTrimVersion = "0.1.0";
    info("fasd-trim v", fasdTrimVersion);

    auto defaultFasdDataFile = "~/.fasd";
    auto fasdDataFileEnvVar  = "_FASD_DATA";
    auto fasdDataFile        = environment.get(fasdDataFileEnvVar);

    if (fasdDataFile is null) {
        fasdDataFile = expandTilde(defaultFasdDataFile);
        info(fasdDataFileEnvVar, " is unset; reading fasd data from ", fasdDataFile, "...");
    } else {
        info("Reading fasd data from ", fasdDataFile, "...");
    }

    auto fasdBackupFile         = fasdDataFile ~ ".bak";
    auto fasdDataFileHandle     = File(fasdDataFile, "r");
    auto fasdTrimWorkFile       = fasdDataFile ~ ".trimmed";
    auto fasdTrimWorkFileHandle = File(fasdTrimWorkFile, "w+");
    auto postCount              = 0;
    auto preCount               = 0;

    foreach (line; fasdDataFileHandle.byLine) {
        preCount++;
        auto resourceLine = split(line, "|");
        if (exists(resourceLine[0])) {
            fasdTrimWorkFileHandle.writeln(line);
            postCount++;
        }
    }

    info(preCount, " entries processed; ", postCount, " entries retained.");
    if (preCount != postCount) {
        rename(fasdDataFile, fasdBackupFile);
        info("A backup of ", fasdDataFile, " was created at ", fasdBackupFile, ".");
        rename(fasdTrimWorkFile, fasdDataFile);
    } else {
        remove(fasdTrimWorkFile);
    }
}
