# fasd-trim

Trims the contents of the data file created by 
[`fasd`](https://github.com/clvv/fasd) to remove entries pointing to 
resources that no longer exist on disk.
